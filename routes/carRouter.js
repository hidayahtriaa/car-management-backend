//import controllers cars
const carController = require("../controllers/carController.js");

// router
const router = require("express").Router();

// use routers
router.post("/addCar", carController.upload, carController.addCar);

router.get("/allCars", carController.getAllCars);

router.put("/:id", carController.updateCar);

router.delete("/:id", carController.deleteCar);

module.exports = router;
