module.exports = (sequelize, DataTypes) => {
  const Car = sequelize.define("car", {
    image: {
      type: DataTypes.STRING,
    },
    car_name: {
      type: DataTypes.STRING,
    },
    car_type: {
      type: DataTypes.STRING,
    },
    price: {
      type: DataTypes.STRING,
    },
    car_size: {
      type: DataTypes.STRING,
    },
  });

  return Car;
};
